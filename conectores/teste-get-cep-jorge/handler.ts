import { middyfy } from '../../libs/lambda';

import Axios from 'axios';

const handler = async (event) => {

  const response = await Axios.get(`https://viacep.com.br/ws/${event.body.cep}/json/`);

  const responseData = response.data;

  return {
    statusCode: 200,
    body: JSON.stringify({
      state: responseData.uf,
      city: responseData.localidade,
      province: responseData.bairro,
      street: responseData.logradouro
    })
  };

};

export const main = middyfy(handler);