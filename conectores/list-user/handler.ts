import { middyfy } from '../../libs/lambda';

import Axios from 'axios';

const PATH = '/platform/user/queries/listUsers';
let response = {};

const listUser = async (event) => {

  try {

    const filters = event.body;

    const searchTerm = filters ? (filters.username || filters.fullName || filters.email || '') : '';

    const url = event.headers['x-platform-environment'] + PATH;

    const responseData = await Axios.get(`${url}?${searchTerm ? `searchTerm=${searchTerm}` : ''}`, {
      headers: { Authorization: event.headers["x-platform-authorization"] }
    });

    response = {
      statusCode: 200,
      body: JSON.stringify(responseData.data)
    };
  } catch (error) {
    console.log(error);
    response = {
      statusCode: error?.response?.status || 400,
      body: error?.response?.data ? JSON.stringify(error?.response?.data) : JSON.stringify(error.message)
    };
  }

  return response;
};

export const main = middyfy(listUser);