import { middyfy } from '../../libs/lambda';

import Axios from 'axios';

const PATH = '/platform/rpa/entities/automation';

const automationsList = async (event) => {
  try {
    const id = event?.queryStringParameters?.id;
    const name = event?.queryStringParameters?.name;
    const fileId = event?.queryStringParameters?.fileId;
    const fileName = event?.queryStringParameters?.fileName;
    const fileVersion = event?.queryStringParameters?.fileVersion;
    const page = event?.queryStringParameters?.page || 0;
    const size = event?.queryStringParameters?.size || 10;
    const filter: string[] = [];

    if (id) {
      filter.push(`id eq '${id}'`);
    }

    if (name) {
      filter.push(`containing(upper(name), upper('${name}'))`);
    }

    if (fileId) {
      filter.push(`fileId eq '${fileId}'`);
    }

    if (fileName) {
      filter.push(`containing(upper(fileName), upper('${fileName}'))`);
    }

    if (fileVersion) {
      filter.push(`fileVersion eq '${fileVersion}'`);
    }

    const url = event.headers['x-platform-environment'] + PATH;

    const responseData = await Axios.get(`${url}${filter.length ? '?filter=' : ''}${filter.length ? filter.join(' AND ') : ''}${filter.length ? '&' : '?'}size=${size}&offset=${page}`, {
      headers: { Authorization: event.headers["x-platform-authorization"] }
    });

    return {
      statusCode: 200,
      body: JSON.stringify(responseData.data)
    };
  } catch (error) {
    console.log(error);
    return {
      statusCode: error?.response?.status || 400,
      body: error?.response?.data ? JSON.stringify(error?.response?.data) : JSON.stringify(error.message)
    };
  }
};

export const main = middyfy(automationsList);