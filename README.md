### Criar conectores

> Para disponibilizar um novo conector é necessário abrir um Merge request para este repositório.

Após clonar o repositório é necessário instalar o Node 14.5.0, e após rodar os comandos:
- em qualquer local `npm install -g serverless`
- acessar a pasta do projeto e rodar o comando `npm install`

O merge request deve conter três arquivos para ser aceito e o novo conector entrar em operação, sendo eles:

- `swagger.yaml`: neste arquivo deve-se conter as informações sobre parâmetros, título e descrição do Conector que serão exibidos dentro do SeniorX.
- `index.ts`: neste arquivo contém a forma de chamada do Conector seja ela um POST, GET, PUT ou DELETE assim como o caminho da requisição.
- `handler.ts`: neste arquivo deve existir um método chamado “handler”, e dentro dele deve ser implementado todas as funções que o conector deve realizar.

Após a criação dos arquivos é necessário manter alguns padrões, como, todos os Conectores devem estar dentro da pasta `conector`, para isso, deve-se criar uma sub-pasta contendo o nome do conetor, este nome deve ser o mesmo utilizado no swagger e no caminho dentro do index.ts, ou seja, caso criar uma pasta `obter-cep`, o swagger na opção paths deve conter um item `/obter-cep` assim como o index.ts o atributo path deve ser `obter-cep`.

Antes de efetivamente commitar os fontes e o conector ficar disponível, é possível rodar localmente usando o comando `sls offline`, e assim testá-lo sem a necessidade da infraestrutura da AWS.

### Definições do Swagger

> O Swagger não está disponível em toda sua totalidade, em relação a
> [documentação  oficial](https://swagger.io/docs/specification/2-0/basic-structure/). Segue abaixo os recursos disponíveis.

**Info estrutura**

 - version: deixar fixado `10.1.2`
 - title: título a ser exibido no SeniorX para o Conector 
 - description: descrição a ser exibido no SeniorX para o Conector 
 - contact: informações para entrar em contato com o desenvolvedor do Conector

**tags**
 - name: nome da categoria a ser exibida no SeniorX

**paths**
 - deve ser o nome da pasta do Conector com um prefixo `/`, por exemplo, caso a pasta do coenctor seja `teste`, ficará `/teste`
 - tipo do método HTTP a ser criado o Conector, são aceitos `POST`, `GET`, `PUT`, `DELETE`

**definitions > bodyData** 

 - type: tipo do parâmetro de entrada, são aceitos somente os seguintes tipos: `string`, `integer`, `double`, `boolean`, `array`
 - description: nome do parâmetro que será exibido no SeniorX
 - enum: campo válido somente para o tipo `string`, para fixar entradas de valores para o Conector dentro do SeniorX

**definitions > outputData**
 - type: tipo do parâmetro de entrada, são aceitos somente os seguintes tipos: `string`, `integer`, `double`, `boolean`
 - description: nome do parâmetro que será exibido no SeniorX 

> Para saídas de dados, ainda não é possível retornar tipos `array` e `enum`.

### Headers padrões
- `x-platform-environment`: Contém a url da plataforma.
- `x-platform-authorization`: Contém o token do SeniorX referênte ao usuário que solicitou o conector.
- `x-platform-tenant`: Contém o tenant da requisição ao conector.
- `x-platform-username`: Contém o e-mail do usuário da requisição ao conector.
